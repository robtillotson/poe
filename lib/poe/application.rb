
require 'rubygems'
require 'poe'
require 'commander/import'
require 'terminal-table/import'
require 'nanowrimo'

program :name, "Poe"
program :version, '0.0.1'
program :description, "A tool for writers"
program :help, "Copyright", "2009 Rob Tillotson <rob@pyrite.org>"

default_command :status

command :list do |c|
  c.action do |args, options|
    book = Poe::Book.new
    
    tbl = table do |t|
      t.headings = 'Ch', 'Name', 'Title', 'Words'
      book.chapters.each do |c|
        t << [c.num, c.name, c.title, c.wordcount]
      end
    end
    puts tbl
  end
end

command :status do |c|
  c.action do |args, options|
    book = Poe::Book.new

    puts
    puts book.printable_info
    puts
    
    if not book.goals.empty?
      puts "Goals:"
      book.goals.each_pair do |k,v|
        puts "  #{k}: #{v.printable_status}"
      end
      puts
    end
    
    tbl = table do |t|
      t << ["Chapters", book.chapters.size]
      t << ["Words", book.wordcount]
    end
    puts tbl
  end
end

command :goal do |c|
  c.action do |args, options|
    book = Poe::Book.new

    args = [:nanowrimo] if args.empty?
    args.each do |arg|
      g = book.goals[arg.to_sym]
      if g.nil?
        puts "This book has no '#{arg}' goal."
        next
      end
      
      tbl = table do |t|
        t << ["Goal", arg]
        t << ["Starts", g.start_time.strftime(Poe::Goal::TIME_FORMAT)] if g.start_time
        t << ["Ends", g.end_time.strftime(Poe::Goal::TIME_FORMAT)] if g.end_time
        t << ["Target Words", g.words] if g.words
        t << ["Base words/day", g.base_words_per_day] if g.start_time and g.end_time and g.words
        t << ["Word Count", book.wordcount]
        t << ["Current words/day", g.actual_words_per_day] if g.start_time
        t << ["Target words/day", g.target_words_per_day] if g.end_time
        
      end
      puts tbl
      
    end
    
  end
end
