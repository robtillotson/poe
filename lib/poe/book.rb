
class Class
  # friendly_attr is like attr_accessor, but makes the "read" accessor also
  # accept an argument, for friendliness in config files etc.
  def friendly_attr(*accessors)
    accessors.each do |m|
      define_method(m) do |*args|
        if args.size > 0
          instance_variable_set("@#{m}", args[0])
        else
          instance_variable_get("@#{m}")
        end
      end

      define_method("#{m}=") do |val|
        instance_variable_set("@#{m}", val)
      end
    end
  end
end

module Poe
  class Book
    attr_accessor :path, :chapters, :goals
    friendly_attr :title, :author, :author_email
    
    def initialize(path='.')
      @path = path
      @goals = {}
      load_poefile
      find_chapters
    end
    
    def find_chapters
      @chapters = []
      
      Dir.glob("#{path}/ch[0-9]*") do |fn|
        m = /ch([0-9]+)[ _]*(.*)\.(.+)/.match(fn)
        @chapters.push(Chapter.new(self, fn, m[1].to_i, m[2], m[3]))
      end
    end

    def load_poefile
      pf = File.join(path,"Poefile")
      instance_eval(File.open(pf).read) if File.readable?(pf)
    end
    
    def wordcount
      @chapters.inject(0) {|sum, ch| sum + ch.wordcount}
    end
    
    # Printable info for commands.
    def printable_info
      "#{title ? title : 'Untitled Book'} by #{author ? author : 'Unknown Author'}#{author_email ? ' <'+author_email+'>' : ''}"
    end
    
    # Goals and nanowrimo
    def goal(name, words=nil, start_time=nil, end_time=nil)
      s = name.to_sym
      if s == :nanowrimo
        y = Time.now.year
        start_time = Time.local(y,11,1)
        end_time = Time.local(y,12,1)
        words = 50000 if words.nil?
      end
      @goals[s] = Poe::Goal.new(self, words, start_time, end_time)
    end  
      
  end
end
