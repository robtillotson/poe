

module Poe
  class Chapter
    attr_accessor :book, :filename, :num, :name, :type, :text

    def initialize(book, filename, num, name, type)
      @book = book
      @filename = filename
      @num = num.to_i
      @name = name
      @type = type.to_sym
    end
    
    def text
      @text ||= File.open(File.join(book.path,filename)).read
    end

    def title
      /^\#+\s+(.*)\s*\#*\s*$/.match(text)[1]
    end
    
    def wordcount
      text.scan(/(\w|-)+/).size
    end
  end
end
