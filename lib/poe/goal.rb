
module Poe
  class Goal
    attr_reader :words, :start_time, :end_time, :book

    TIME_FORMAT = '%m/%d/%Y %H:%M:%S'
    
    DAY_SECS = 86400
    HOUR_SECS = 3600
    
    def initialize(book, words=nil, start_time=nil, end_time=nil)
      @book = book
      @words = words
      @start_time = start_time
      @end_time = end_time
    end
    
    def printable_info
      l = []
      l << ["#{words} words"] if not words.nil?
      l << ["starting #{start_time.strftime(TIME_FORMAT)}"] if not start_time.nil?
      l << ["ending #{end_time.strftime(TIME_FORMAT)}"] if not end_time.nil?
      l.join(', ')
    end

    def actual_words_per_day
      return 0 if not start_time or Time.now < start_time
      book.wordcount / (((Time.now - start_time)/DAY_SECS).ceil)
    end
    
    def target_words_per_day
      return 0 if not end_time or not words or words < 1
      t = (not start_time or Time.now >= start_time) ? Time.now : start_time
      (words - book.wordcount) / (((end_time - t)/DAY_SECS).floor)
    end
    
    def base_words_per_day
      return 0 if not end_time or not words or words < 1 or not start_time
      words / (((end_time - start_time)/DAY_SECS).floor)
    end
    
        
    def printable_status
      t = Time.now
      bwc = book.wordcount if words
      case
      when (start_time and t < start_time)
        "starts #{start_time.strftime(TIME_FORMAT)}"
      when (end_time and t >= end_time)
        "ended #{end_time.strftime(TIME_FORMAT)}" + (words ? ((bwc >= words) ? "SUCCEEDED!" : "FAILED") + " (#{bwc} words)" : "")
      when (words and start_time and end_time)
        "#{bwc}/#{words} words (#{(((bwc.to_f/words)*1000.0).floor)/10.0}%), #{target_words_per_day}/day to finish"
      end
    end
    
  end

end
